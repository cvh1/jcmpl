This repository purpose is to create maven artifacts for the jCMPL project.

If you're looking for jCMPL, please visit http://www.coliop.org.

## License

This maven project is published under LGPLv3 like the originating project.

## jCMPL

jCMPL is the CMPL API for Java.
 
The main idea of this API is
 
* to define sets and parameters within the user application, 
* to start and control the solving process and 
* to read the solution(s) into the application if the problem is feasible.
 
All variables, objective functions and constraints are defined in CMPL. 
These functionalities can be used with a local CMPL installation or a CMPLServer.

For further information please visit the CMPL website (www.coliop.org). 

## Documentation

See http://www.coliop.org for documentation on jCMPL. This website features a 
User's Manual in pdf format. See also:
 	
https://projects.coin-or.org/Cmpl
